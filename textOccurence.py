#!/usr/bin/env python

import argparse
import glob
import os
import sys
import json

class textOccurence:
    """
    A class to parse text and occurence

    ...

    Attributes
    ----------
    dictOfResults : dict
        Dict where results are stored
    keepChar : list
        Non AlphaNumeric characters will still be count as Alphanumeric
    caseSensitive : bool
        Enable or disable case sensitivity
    keepNonAlNum : bool
        Enable or disable the count of non-alphanumeric character

    Methods
    -------
    countLetters()
        Count all letters in text(s)
    countWords()
        Count all words in text
    countPolygrams(sizeOfPolygrams, glide=False)
        Count all polygrams of a given size in text
    """

    def __init__(self, listOfFiles: list,
                 case: bool = False,
                 nonAlNum: bool = False,
                 position: bool = False,
                 atomic: bool = False):
        """
        Parameters
        ----------
        listOfFiles : list
            List of files to parse
        case : bool, optional
            Case Sensitivity
        nonAlNum : bool, optional
            Count non alpha-numeric character
        """
        self.listOfFiles = listOfFiles
        self.dictOfResults = {}
        self.keepChar = ['’', '-', '\'']
        self.caseSensitive = case
        self.keepNonAlNum = nonAlNum
        self.store_position = position
        self.atomic = atomic
        self.l_already_counted = False
        self.w_already_counted = False
        self.p_already_counted = False
        # Create dicts skeletons
        if self.atomic is True:
            for filename in self.listOfFiles:
                self.dictOfResults.update({filename: {}})
        else:
            self.dictOfResults.update({'Files': self.listOfFiles})


    def countLetters(self):
        """Count all letters in text
        """
        if self.atomic is False:
            self.dictOfResults['Letters'] = {}
        for currentFile in self.listOfFiles:
            # Index used to store the letter position
            i = 0
            # Check if is file and permitted
            if (os.access(currentFile, os.R_OK) and
                    os.path.isfile(currentFile)):
                # Create the letters subkey in dict
                if self.atomic is True:
                    self.dictOfResults[currentFile]['Letters'] = {}
                # create a sub dict to prevent using big nested dict
                # And loose uneeded performance
                dict_to_insert = {}
                # Use "with" to handle Big Files
                with open(currentFile, "r") as f:
                    while True:
                        char = None
                        try:
                            # read char one by one
                            char = f.read(1)
                        except UnicodeDecodeError:
                            print("File Error: " +
                                    currentFile + ' is not Raw Text')
                            self.listOfFiles.remove(currentFile)
                            break
                        except IOError:
                            self.listOfFiles.remove(currentFile)
                            print("IO File Error: " + currentFile)
                            break
                        if char is not None:
                            # If not end of file
                            if char != '':
                                # Lowercase if not case sensitive
                                if self.caseSensitive is False:
                                    char = char.lower()
                                # Check if char is alphanumeric
                                if self.keepNonAlNum is False:
                                    if char.isalnum() or char in self.keepChar:
                                        pass
                                    else:
                                        i += 1
                                        continue
                                # If key is already in dict
                                if dict_to_insert.get(char) is not None:
                                    dict_to_insert[char]['number'] += 1
                                    if self.store_position is True:
                                        dict_to_insert[char]['position'].append(i)
                                else:
                                    if self.store_position is True:
                                        dictToAdd = {char: {'number': 1, 'position': [i]}}
                                    else:
                                        dictToAdd = {char: {'number': 1}}
                                    dict_to_insert.update(dictToAdd)
                                i += 1
                            # End Of File
                            else:
                                # Insert dict in our results
                                if self.atomic is True:
                                    self.dictOfResults[currentFile]['Letters'].update(dict_to_insert)
                                else:
                                    for letter in dict_to_insert:
                                        if letter in self.dictOfResults['Letters']:
                                            self.dictOfResults['Letters'][letter]['number'] += dict_to_insert[letter]['number']
                                            if self.store_position is True:
                                                for position in dict_to_insert[letter]['position']:
                                                    self.dictOfResults['Letters'][letter]['position'].append(position)
                                        else:
                                            self.dictOfResults['Letters'].update({letter: dict_to_insert[letter]})
                                break
            # Is not a file or we cannot read
            else:
                self.listOfFiles.remove(currentFile)
                print("File Error " + currentFile)
        self.l_already_counted = True


    def countWords(self):
        """Count all words in text
        """
        if self.atomic is False:
            self.dictOfResults['Words'] = {}
        for currentFile in self.listOfFiles:
            text = None
            # To prevent dict update inserting in all top level dict
            if (os.access(currentFile, os.R_OK) and
                    os.path.isfile(currentFile)):
                # Create the words subkey in dict
                if self.atomic is True:
                    self.dictOfResults[currentFile]['Words'] = {}
                # create a sub dict to prevent using big nested dict
                # And loose uneeded performance
                dict_to_insert = {}
                with open(currentFile, "r") as f:
                    try:
                        # read all the file
                        text = f.read()
                    except IOError:
                        self.listOfFiles.remove(currentFile)
                        print("IO File Error: " + currentFile)
                    except UnicodeDecodeError:
                        self.listOfFiles.remove(currentFile)
                        print("File Error: " + currentFile +
                              ' is not Raw Text')
            else:
                self.listOfFiles.remove(currentFile)
                print("File Error: " + currentFile)
            if text is not None:
                # Put all words from the text in a list
                if self.store_position is True:
                    listOfWords = text.split(' ')
                else:
                    listOfWords = text.split()
                # Index used to store word position
                i = 0
                word_index = 0
                for word in listOfWords:
                    wordSize = len(word)
                    # Get the word positions from given index
                    if self.store_position is True:
                        word_position = listOfWords.index(word, i) + word_index
                    # Lowercase if not case sensitive
                    if self.caseSensitive is False:
                        word = word.lower()
                    # Remove non alphanumeric but keeped char from Words
                    if self.keepNonAlNum is False:
                        word_filter = filter(lambda x: x.isalnum()
                                             or x in self.keepChar, word)
                        word = "".join(word_filter)
                    # Don't store spaces
                    if word != '':
                        # Word already in dict
                        if dict_to_insert.get(word) is not None:
                            dict_to_insert[word]['number'] += 1
                            if self.store_position is True:
                                dict_to_insert[word]['position'].append(word_position)
                        # Word not in dict
                        else:
                            if self.store_position is True:
                                dictToAdd = {word: {'number': 1, 'position': [word_position]}}
                            else:
                                dictToAdd = {word: {'number': 1}}
                                #dictToAdd[word].update({'position': [word_position]})
                            dict_to_insert.update(dictToAdd)
                    i += 1 #+ size_difference
                    word_index += wordSize
            if self.atomic is True:
                self.dictOfResults[currentFile]['Words'].update(dict_to_insert)
            else:
                for word in dict_to_insert:
                    if word in self.dictOfResults['Words']:
                        self.dictOfResults['Words'][word]['number'] += dict_to_insert[word]['number']
                        if self.store_position is True:
                            for position in dict_to_insert[word]['position']:
                                self.dictOfResults['Words'][word]['position'].append(position)
                    else:
                        self.dictOfResults['Words'].update({word: dict_to_insert[word]})
        self.w_already_counted = True


    def countPolygrams(self, size: int, glide: bool = False):
        """Count all polygrams in text

        Parameters
        ----------
        size : int
            The size of polygrams to count
        glide : bool, optional
            Count characters only one time
        """
        # We start by reading all words if needed
        if self.w_already_counted is False:
            self.countWords()
        s = int(size)
        if self.atomic is False:
            self.dictOfResults['Polygrams'] = {}
        for currentFile in self.listOfFiles:
            # Create the polygrams subkey in dict
            if self.atomic is True:
                self.dictOfResults[currentFile]['Polygrams'] = {}
            # create a sub dict to prevent using big nested dict
            # And loose uneeded performance
            dict_to_insert = {}
            # Since the dict will generated by other method
            # we need to get them
            if self.atomic is True:
                dict_to_parse = self.dictOfResults[currentFile]['Words']
            else:
                dict_to_parse = self.dictOfResults['Words']
            for word in dict_to_parse:
                for i in range(len(word)):
                    if glide is False:
                        poly = word[i:i+s]
                    else:
                        poly = word[i*s: i*s+s]
                    if len(poly) == size:
                        if poly in dict_to_insert:
                            dict_to_insert[poly]['number'] += dict_to_parse[word]['number']
                            if self.store_position is True:
                                dict_to_insert[poly]['position'].append([x + i for x in dict_to_parse[word]['position']])
                        else:
                            dict_to_insert.update({poly: {}})
                            dict_to_insert[poly].update({'number': dict_to_parse[word]['number']})
                            if self.store_position is True:
                                dict_to_insert[poly].update({'position': [x + i for x in dict_to_parse[word]['position']]})
            if self.atomic is True:
                self.dictOfResults[currentFile]['Polygrams'].update(dict_to_insert)
            else:
                # Insert dict in our results
                if self.atomic is True:
                    self.dictOfResults[currentFile]['Polygrams'].update(dict_to_insert)
                else:
                    for poly in dict_to_insert:
                        if poly in self.dictOfResults['Polygrams']:
                            self.dictOfResults['Polygrams'][poly]['number'] += dict_to_insert[poly]['number']
                            if self.store_position is True:
                                for position in dict_to_insert[poly]['position']:
                                    self.dictOfResults['Polygrams'][poly]['position'].append(position)
                        else:
                            self.dictOfResults['Polygrams'].update({poly: dict_to_insert[poly]})
        self.p_already_counted = True

def addFileToList(listToAppend: list, fileToAdd: str, recursive: bool = False):
    """Add File to a list if file is directory follow them or not
    Symlinks will not be followed

    Parameters
    ----------
    listToAppend : list
        The list where to files are added
    fileToAdd : str
        The file to add to list
    recursive : bool, optional
        Open directory recursively or not
    """
    # Check permissions
    if os.access(fileToAdd, os.R_OK):
        if os.path.isfile(fileToAdd):
            with open(fileToAdd, "r") as f:
                try:
                    # try if we can read this file
                    f.read(1)
                except UnicodeDecodeError:
                    return
                except IOError:
                    return
                listToAppend.append(fileToAdd)
        # If it's a directory parse it
        elif os.path.isdir(fileToAdd):
            # Don't follow symlink
            if os.path.islink(fileToAdd) is False:
                for elem in glob.glob(fileToAdd + '/*'):
                    addFileToList(listToAppend, elem, True)


def main():

    # read commandline arguments, first
    parser = argparse.ArgumentParser(description="Count occurrences in RAW text files (not work with PDF,Binary, etc.)")

    parser.add_argument('toOpen', metavar='N', type=str, nargs='*',
                        help='Files or directory to parse if not parse current directory')

    parser.add_argument('-R', '--recursive', required=False, action='store_true',
                        help="Parse directories recursively")

    parser.add_argument('--position', required=False, action='store_true',
                        help="Store position of letetrs, words, and polygrams")

    parser.add_argument('-q', '--quiet', required=False, action='store_true',
                        help="Prevent print result on screen, errors still printed")

    parser.add_argument('-c', '--case', required=False, action='store_true',
                        help="Enable case sensitive")

    parser.add_argument('-A', '--atomic', required=False, action='store_true',
                        help="When a directory is used give res by files not global")

    parser.add_argument('--json', required=False, action='store_true',
                        help="Save result in json file")

    parser.add_argument('-a', '--all', required=False, action='store_true',
                        help="Keep and count non alphanumeric (Ex: 'test,'  will be count separately from 'test' )\nNote:characters '’' and '-' will still be count as part of word")

    parser.add_argument('-l', '--letter', required=False,
                        help="Count occurences of a given letter")

    parser.add_argument('-L', '--letters', required=False, action='store_true',
                        help="Count and sort occurences of all letters")

    parser.add_argument('-w', '--word', required=False,
                        help="Count occurences of a given word")

    parser.add_argument('-W', '--words', required=False, action='store_true',
                        help="Count and sort occurences of all word")

    parser.add_argument('-P', '--polygram', required=False, type=int,
                        help='Count all polygrams of given size (Ex with size 2: example {ex, xa, am, mp, pl, le})')

    parser.add_argument('-s', '--sequence', required=False,
                        help="Count occurences of given sequence")

    parser.add_argument('-G', '--glide', required=False, action='store_true',
                        help="Use with polygrams or sequence, each char will be count only one time (Ex with size 2: example {ex, am, pl})")

    # Parse Args
    args = parser.parse_args()

    # Get list of files to Parse
    filesToParse = []
    for item in args.toOpen:
        addFileToList(filesToParse, item, args.recursive)

    # Instance Class
    texts = textOccurence(filesToParse, args.case, args.all, args.position, args.atomic)

    # If non case sensitive lower inputs
    if args.case is False:
        if args.letter is not None:
            args.letter = args.letter.lower()
        if args.word is not None:
            args.word = args.word.lower()
        if args.sequence is not None:
            args.sequence = args.sequence.lower()

    # Count only needed
    if args.letter is not None or args.letters is True:
        if texts.l_already_counted is False:
            texts.countLetters()
    if args.word is not None or args.words is True:
        if texts.w_already_counted is False:
            texts.countWords()
    if args.polygram is not None:
        if texts.p_already_counted is False:
            texts.countPolygrams(int(args.polygram), args.glide)
    if args.sequence is not None:
        if texts.p_already_counted is False:
            texts.countPolygrams(len(args.sequence), args.glide)
        elif args.polygram is not None:
            if int(args.polygram) != len(args.sequence):
                texts.countPolygrams(len(args.sequence), args.glide)

    # Print results if wanted
    if args.quiet is False:
        # Print result by file:
        if args.atomic is True:
            for filename in texts.listOfFiles:
                print('In File: '+filename)
                # Print wanted letter
                if args.letter is not None:
                    if args.letter in texts.dictOfResults[filename]['Letters']:
                        res_str = str(texts.dictOfResults[filename]['Letters'][args.letter]['number'])
                    else:
                        res_str = '0'
                    res_str += ' Occurence of letter: '+ args.letter
                    print(res_str)
                if args.letters is True:
                    print(texts.dictOfResults[filename]['Letters'])
                if args.word is not None:
                    if args.word in texts.dictOfResults[filename]['Words']:
                        res_str = str(texts.dictOfResults[filename]['Words'][args.word]['number'])
                    else:
                        res_str = '0'
                    res_str += ' Occurence of word: '+ args.word
                    print(res_str)
                if args.words is True:
                    print(texts.dictOfResults[filename]['Words'])
                if args.polygram is not None:
                    print(texts.dictOfResults[filename]['Polygrams'])
                if args.sequence is not None:
                    if args.sequence in texts.dictOfResults[filename]['Polygrams']:
                        res_str = str(texts.dictOfResults[filename]['Polygrams'][args.sequence]['number'])
                    else:
                        res_str = '0'
                    res_str += ' Occurence of sequence: '+ args.sequence
                    print(res_str)
        # Print all results
        else:
            print('In Files: '+str(texts.listOfFiles))
            if args.letter is not None:
                if args.letter in texts.dictOfResults['Letters']:
                    res_str = str(texts.dictOfResults['Letters'][args.letter]['number'])
                else:
                    res_str = '0'
                res_str += ' Occurence of letter: '+ args.letter
                print(res_str)
            if args.letters is True:
                print(texts.dictOfResults['Letters'])
            if args.word is not None:
                if args.word in texts.dictOfResults['Words']:
                    res_str = str(texts.dictOfResults['Words'][args.word]['number'])
                else:
                    res_str = '0'
                res_str += ' Occurence of word: '+ args.word
                print(res_str)
            if args.words is True:
                print(texts.dictOfResults['Words'])
            if args.polygram is not None:
                print(texts.dictOfResults['Polygrams'])
            if args.sequence is not None:
                if args.sequence in texts.dictOfResults['Polygrams']:
                    res_str = str(texts.dictOfResults['Polygrams'][args.sequence]['number'])
                else:
                    res_str = '0'
                res_str += ' Occurence of sequence: '+ args.sequence
                print(res_str)

    # Check if we need to save result in JSON
    if args.json is True:
        with open('data.json', 'w') as fp:
            json.dump(texts.dictOfResults, fp)
    exit()


if __name__ == "__main__":
    main()
